#!/bin/bash

if [ $# -ne 1 ]; then
	echo "Usage: $0 <text.parameter>"
else
	if [[ $1 =~ ^[0-9]+$ ]]; then
		echo "Invalid input: $1 is a number"
	else
		echo "$1"
	fi
fi
