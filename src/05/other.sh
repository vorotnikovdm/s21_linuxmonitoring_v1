#!/bin/bash

function print_sysinfo {
	start_time=$(date +%s.%N)
	echo "$time_n"
	echo "Total number of folders (including all nested ones) = $(($(find "$1" -type d | wc -l) - 1 ))"
	echo "TOP 5 folders of maximum size arranged in descending order (path and size):"
	top_5 "$1"
	echo "Total number of files = $(find "$1" -type f | wc -l)"
	echo "Number of:"
	echo "Configuration files (with the .conf extension) = $(find "$1" -type f -name "*.conf" | wc -l)"
	echo "Text files = $(find "$1" -type f -name "*.txt" | wc -l)"
	echo "Executable files = $(find "$1" -type f -executable | wc -l)"
	echo "Log files (with the extension .log) = $(find "$1" -type f -name "*.log" | wc -l)"
	echo "Archive files = $(find "$1" -type f -name "*.tar" -o -name "*.zip" | wc -l)"
	echo "Symbolic links = $(find "$1" -type l | wc -l)"
	echo "TOP 10 files of maximum size arranged in descending order (path, size and type):"
	top_10 "$1"
	echo "TOP 10 executable files of the maximum size arranged in descending order (path, size and MD5 hash of file)"
	top_10_executable "$1"
	end_time=$(date +%s.%N)
	execution_time=$(echo "$end_time - $start_time" | bc)
	echo "Script execution time (in seconds) = $execution_time"
}

function top_5 {
	directory="$1"
	declare -a path
	declare -a size
	while IFS= read -r line; do
		path+=("$(echo "$line" | awk '{printf $2}')")
		size+=("$(echo "$line" | awk '{printf $1}')")
	done < <(du -h --exclude=/proc "$directory" | sort -rh | head -n 6)
	path_size="${#path[@]}"
	for ((i = 1; i < path_size; i++)); do
		echo "$i - ${path[i]}, ${size[i]}"
	done
}

function top_10 {
	directory="$1"
	declare -a path
	declare -a size
	declare -a filetype
	declare -a extension
	while IFS= read -r line; do
		path+=("$(echo "$line" | awk '{printf $2}')")
		size+=("$(echo "$line" | awk '{printf $1}')")
		filetype+=("$(echo "$line" | awk '{printf $2}')")
	done < <(find "$directory" -type d -name proc -prune -o -type f -exec du -h {} + | sort -rh | head -n 10)
	amount="${#path[@]}"
	for temp in "${filetype[@]}"; do
		temp_extension="${temp##*.}"
		extension+=("$temp_extension")
	done
	for ((i = 0; i < amount; i++)); do
		echo "$(($i + 1)) - ${path[i]}, ${size[i]}, ${extension[i]}"
	done
}

function top_10_executable {
	directory="$1"
	declare -a path
	declare -a size
	declare -a file_hash
	while IFS= read -r line; do
		path+=("$(echo "$line" | awk '{printf $2}')")
		size+=("$(echo "$line" | awk '{printf $1}')")
	done < <(find "$directory" -type d -name proc -prune -o -type f -executable -exec du -h {} + | sort -rh | head -n 10)
	amount="${#path[@]}"
	for ((j = 0; j < amount; j++)); do
		file_hash+=("$(md5sum "${path[j]}" | awk '{printf $1}')")
	done
	for ((i = 0; i < amount; i++)); do
		echo "$(($i + 1)) - ${path[i]}, ${size[i]}, ${file_hash[i]}"
	done
}
