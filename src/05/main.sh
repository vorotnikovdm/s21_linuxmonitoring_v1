#!/bin/bash

. ./other.sh

if [ $# -ne 1 ]; then
	echo "Usage: $0 <directory>"
else
	if [ ! -d "$1" ]; then
		echo "Error: '$1' is not a valid directory"
	else
		print_sysinfo "$1"
	fi
fi
