#!/bin/bash

function print_sysinfo {
HOSTNAME=$(hostname)
TIMEZONE=$(timedatectl show --property=Timezone --value)
USER=$(whoami)
OS=$(lsb_release -d | cut -f2-)
DATE=$(date +"%d %B %Y %T")
UPTIME=$(uptime -p)
UPTIME_SEC=$(cat /proc/uptime | awk '{print $1}')
IP=$(hostname -I | awk '{print $1}')
MASK=$(ifconfig | grep "$IP" | awk '{printf $4}')
GATEWAY=$(ip route | awk '/default/ {print $3}')
RAM_TOTAL=$(free -m | awk '/Mem:/ {printf "%.3f", $2/1024}')
RAM_USED=$(free -m | awk '/Mem:/ {printf "%.3f", $3/1024}')
RAM_FREE=$(free -m | awk '/Mem:/ {printf "%.3f",$4/1024}')
SPACE_ROOT=$(df -k / | awk 'NR==2 {printf "%.2f", $2/1024}')
SPACE_ROOT_USED=$(df -k / | awk 'NR==2 {printf "%.2f", $3/1024}')
SPACE_ROOT_FREE=$(df -k / | awk 'NR==2 {printf "%.2f", $4/1024}')
bg_colors=("47" "41" "42" "44" "45" "40")
font_colors=("37" "31" "32" "34" "35" "30")
bg1="${bg_colors[$1 - 1]}"
font1="${font_colors[$2 - 1]}"
bg2="${bg_colors[$3 - 1]}"
font2="${font_colors[$4 - 1]}"

echo -e "\033[${bg1};${font1}mHOSTNAME\033[0m = \033[${bg2};${font2}m$HOSTNAME\033[0m"
echo -e "\033[${bg1};${font1}mTIMEZONE\033[0m = \033[${bg2};${font2}m$TIMEZONE UTC $(date +"%:::z")\033[0m"
echo -e "\033[${bg1};${font1}mUSER\033[0m = \033[${bg2};${font2}m$USER\033[0m"
echo -e "\033[${bg1};${font1}mOS\033[0m = \033[${bg2};${font2}m$OS\033[0m"
echo -e "\033[${bg1};${font1}mDATE\033[0m = \033[${bg2};${font2}m$DATE\033[0m"
echo -e "\033[${bg1};${font1}mUPTIME\033[0m = \033[${bg2};${font2}m$UPTIME\033[0m"
echo -e "\033[${bg1};${font1}mUPTIME_SEC\033[0m = \033[${bg2};${font2}m$UPTIME_SEC\033[0m"
echo -e "\033[${bg1};${font1}mIP\033[0m = \033[${bg2};${font2}m$IP\033[0m"
echo -e "\033[${bg1};${font1}mMASK\033[0m = \033[${bg2};${font2}m$MASK\033[0m"
echo -e "\033[${bg1};${font1}mGATEWAY\033[0m = \033[${bg2};${font2}m$GATEWAY\033[0m"
echo -e "\033[${bg1};${font1}mRAM_TOTAL\033[0m = \033[${bg2};${font2}m${RAM_TOTAL} GB\033[0m"
echo -e "\033[${bg1};${font1}mRAM_USED\033[0m = \033[${bg2};${font2}m${RAM_USED} GB\033[0m"
echo -e "\033[${bg1};${font1}mRAM_FREE\033[0m = \033[${bg2};${font2}m${RAM_FREE} GB\033[0m"
echo -e "\033[${bg1};${font1}mSPACE_ROOT\033[0m = \033[${bg2};${font2}m${SPACE_ROOT} MB\033[0m"
echo -e "\033[${bg1};${font1}mSPACE_ROOT_USED\033[0m = \033[${bg2};${font2}m${SPACE_ROOT_USED} MB\033[0m"
echo -e "\033[${bg1};${font1}mSPACE_ROOT_FREE\033[0m = \033[${bg2};${font2}m${SPACE_ROOT_FREE} MB\033[0m"
}

function colors_check {
	for var in "$@"
	do
		if [[ ! "$var" =~ ^[1-6]$ ]]; then
			echo "Invalid color(s): Color parameters must be in the range of 1 to 6"
		        return 1
		fi
	done
	return 0
}

function colors_match {
	if [[ "$1" == "$2" ]] || [[ "$3" == "$4" ]]; then
		echo "Font and backgound colors must not match"
		return 1
	fi
	return 0
}
