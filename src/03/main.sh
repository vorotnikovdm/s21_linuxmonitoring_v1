#!/bin/bash

. ./other.sh

bg_color1=$1
font_color1=$2
bg_color2=$3
font_color2=$4
flag1=1
flag2=1
parameter_count=$#

while [ $flag1 -eq 1 ] || [ $flag2 -eq 1 ]
do
	if [ $parameter_count -ne 4 ]; then
		echo "Usage: $0 <bg_color1> <font_color1> <bg_color2> <font_color2>"
		exit 1
	else
		colors_check "$bg_color1" "$font_color1" "$bg_color2" "$font_color2"
		flag1=$?
		if [ $flag1 -eq 1 ]; then
			exit 1
		else
			colors_match "$bg_color1" "$font_color1" "$bg_color2" "$font_color2"
			flag2=$?
			if [ $flag2 -eq 1 ]; then
				read -p "Wrong parameters, try again: " -a colors
				parameter_count=${#colors[@]}
				bg_color1=${colors[0]}
				font_color1=${colors[1]}
				bg_color2=${colors[2]}
				font_color2=${colors[3]}
			fi
		fi
	fi
done
print_sysinfo "$bg_color1" "$font_color1" "$bg_color2" "$font_color2"
