#!/bin/bash

. ./other.sh

config_file="colors.conf"
bg_color1=5
font_color1=1
bg_color2=5
font_color2=1
bg1_check=0
font1_check=0
bg2_check=0
font2_check=0

if [ $# -ne 0 ]; then
	echo "Usage: $0"
else
	if [ -f "$config_file" ]; then
		source "$config_file"
		if [ ! -z "$column1_background" ]; then
			bg_color1=$column1_background
		else
			bg1_check=1
		fi
		if [ ! -z "$column1_font_color" ]; then
			font_color1=$column1_font_color
		else
			font1_check=1
		fi
		if [ ! -z "$column2_background" ]; then
			bg_color2=$column2_background
		else
			bg2_check=1
		fi
		if [ ! -z "$column2_font_color" ]; then
			font_color2=$column2_font_color
		else
			font2_check=1
		fi
	else
		echo "Config file not found. Using default color scheme"
		bg1_check=1
		font1_check=1
		bg2_check=1
		font2_check=1
	fi
	if colors_check "$bg_color1" "$font_color1" "$bg_color2" "$font_color2"; then
		if colors_match "$bg_color1" "$font_color1" "$bg_color2" "$font_color2"; then
			print_sysinfo "$bg_color1" "$font_color1" "$bg_color2" "$font_color2"
			print_color_scheme "$bg_color1" "$font_color1" "$bg_color2" "$font_color2" "$bg1_check" "$font1_check" "$bg2_check" "$font2_check"
		fi
	fi
fi
