#!/bin/bash

function print_sysinfo {
HOSTNAME=$(hostname)
TIMEZONE=$(timedatectl show --property=Timezone --value)
USER=$(whoami)
OS=$(lsb_release -d | cut -f2-)
DATE=$(date +"%d %B %Y %T")
UPTIME=$(uptime -p)
UPTIME_SEC=$(cat /proc/uptime | awk '{print $1}')
IP=$(hostname -I | awk '{print $1}')
MASK=$(ifconfig | grep "$IP" | awk '{printf $4}')
GATEWAY=$(ip route | awk ' /default/ {printf $3}')
RAM_TOTAL=$(free -m | awk '/Mem:/ {printf "%.3f", $2/1024}')
RAM_USED=$(free -m | awk '/Mem:/ {printf "%.3f", $3/1024}')
RAM_FREE=$(free -m | awk '/Mem:/ {printf "%.3f",$4/1024}')
SPACE_ROOT=$(df -k / | awk 'NR==2 {printf "%.2f", $2/1024}')
SPACE_ROOT_USED=$(df -k / | awk 'NR==2 {printf "%.2f", $3/1024}')
SPACE_ROOT_FREE=$(df -k / | awk 'NR==2 {printf "%.2f", $4/1024}')

echo "HOSTNAME = $HOSTNAME"
echo "TIMEZONE = $TIMEZONE UTC $(date +"%:::z")"
echo "USER = $USER"
echo "OS = $OS"
echo "DATE = $DATE"
echo "UPTIME = $UPTIME"
echo "UPTIME_SEC = $UPTIME_SEC"
echo "IP = $IP"
echo "MASK = $MASK"
echo "GATEWAY = $GATEWAY"
echo "RAM_TOTAL = ${RAM_TOTAL} GB"
echo "RAM_USED = ${RAM_USED} GB"
echo "RAM_FREE = ${RAM_FREE} GB"
echo "SPACE_ROOT = ${SPACE_ROOT} MB"
echo "SPACE_ROOT_USED = ${SPACE_ROOT_USED} MB"
echo "SPACE_ROOT_FREE = ${SPACE_ROOT_FREE} MB"
}

function save_file {
read -p "Do you want to save this information to a file? (Y/N): " choice

if [ "$choice" == "Y" ] || [ "$choice" == "y" ]; then
	filename="`date +%d_%m_%y_%H_%M_%S`.status"
	touch $filename
	IFS=$'\n'
	for var in $(print_sysinfo)
	do
		echo $var >> $filename
	done
	echo "Data has been saved to $filename"
else
	echo "Data not saved"
fi
}
